package ce305;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import static org.antlr.v4.runtime.CharStreams.fromFileName;

import java.io.IOException;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Unit test for simple App.
 */
public class MainTest {
    private final static Path FILE = Paths.get("input/add01.txt");

    @Test
    public void shouldAnswerWithTrue() throws IOException {
        CharStream cs = fromFileName(FILE.toAbsolutePath().toString()); // read the input file
        Lexer lexer = new Lexer(cs); // create a lexer object
        org.antlr.v4.runtime.CommonTokenStream tokens = new CommonTokenStream(lexer); // scan stream for tokens
        Parser parser = new Parser(tokens);
        ParseTree tree = parser.start();

        String pprint = new PprintVisitor().visit(tree);
        System.out.printf("%s\n", pprint);

        Double answer = new EvalVisitor().visit(tree);
        System.out.printf("%s\n", answer);
    }
}
