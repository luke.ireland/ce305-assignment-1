grammar Expr;

start: statements;
statements: statement*;
statement: (numcond | boolcond | exp | assign);

exp:
	left = exp op = (MUL | DIV) right = exp		# multiplicative
	| left = exp op = (ADD | SUB) right = exp	# additive
	| SUB? LPAREN exp RPAREN					# parentheses
	| SUB? NUMBER								# num
	| ID										# id;

assign: ID '=' (numcond | boolcond | exp) SEMICOLON?;
numcond: left = exp op = NUMOPS right = exp SEMICOLON?;
boolcond:
	left = numcond op = BOOLOPS right = numcond SEMICOLON?;

NUMOPS: (EQ | NE | GT | LT | GE | LE);
BOOLOPS: (AND | OR);

NUMBER: [0-9]+('.'[0-9]+)?;
ID: [a-zA-Z]+;
MUL: '*';
DIV: '/';
ADD: '+';
SUB: '-';
EQ: '==';
NE: '!=';
GT: '>';
LT: '<';
GE: '>=';
LE: '<=';
AND: '&';
OR: '|';
LPAREN: '(';
RPAREN: ')';
SEMICOLON: ';';

WS: [ \t\r\n]+ -> skip; // skip white space
