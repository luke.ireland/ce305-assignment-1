package ce305;

public class PprintVisitor extends ExprBaseVisitor<String> {

    @Override
    public String visitStart(ExprParser.StartContext ctx) {
        return this.visit(ctx.statements());
    }

    @Override
    public String visitStatements(ExprParser.StatementsContext ctx) {
        String result = "";
        for (ExprParser.StatementContext e : ctx.statement()) {
            result += this.visit(e) + "\n";
        }
        return result + "\n";
    }

    // Print variable and value of assignment
    @Override
    public String visitAssign(ExprParser.AssignContext ctx) {
        String var = ctx.ID().getText();
        String value;
        if (ctx.boolcond() != null){
            value = this.visit(ctx.boolcond());
        }
        else if (ctx.numcond() != null){
            value = this.visit(ctx.numcond());
        }
        else{
            value = this.visit(ctx.exp());
        }
        String result = var + " = " + value;
        return result;
    }

    // Print pretty expressions and comparison operator
    @Override
    public String visitNumcond(ExprParser.NumcondContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        return left + " " + ctx.NUMOPS().getText() + " " + right;
    }

    // Print pretty expressions and comparison operator
    @Override
    public String visitBoolcond(ExprParser.BoolcondContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        return left + " " + ctx.BOOLOPS().getText() + " " + right;
    }

    @Override
    public String visitMultiplicative(ExprParser.MultiplicativeContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        return left + " " + ctx.op.getText() + " " + right;
    }

    @Override
    public String visitAdditive(ExprParser.AdditiveContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        return left + " " + ctx.op.getText() + " " + right;
    }

    @Override
    public String visitNum(ExprParser.NumContext ctx) {
        return ctx.SUB().getText()+ctx.NUMBER().getText();
    }

    @Override
    public String visitParentheses(ExprParser.ParenthesesContext ctx) {
        return (ctx.SUB() == null ? "" : "-") + "(" + this.visit(ctx.exp()) + ")";
    }

    @Override
    public String visitId(ExprParser.IdContext ctx) {
        return ctx.ID().getText();
    }
}
