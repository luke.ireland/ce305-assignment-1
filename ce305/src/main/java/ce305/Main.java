package ce305;

import static org.antlr.v4.runtime.CharStreams.fromFileName;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.Arrays;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.gui.TreeViewer;

import javax.swing.JPanel;
import javax.swing.JFrame;

public class Main {
    private final static String DIR = "input/";

    public static void showTree(ExprParser parser, ParseTree tree) {
        // show AST in GUI
        JFrame frame = new JFrame("Antlr AST");
        JPanel panel = new JPanel();
        TreeViewer viewer = new TreeViewer(Arrays.asList(parser.getRuleNames()), tree);
        viewer.setScale(1.5); // Scale a little
        panel.add(viewer);
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) throws IOException {
        CharStream cs = fromFileName(DIR + "negative.txt"); // read the input file
        ExprLexer lexer = new ExprLexer(cs); // create a lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer); // scan stream for tokens
        ExprParser parser = new ExprParser(tokens); // Create a parser using tokens
        ParseTree tree = parser.start(); // Create AST parse tree from parser
        showTree(parser, tree);

        String pprint = new PprintVisitor().visit(tree); // Build string by traversing AST
        System.out.printf("\nPprint:\n%s\n", pprint);

        HashMap<String, Double> varMap = new HashMap<>();
        EvalVisitor eval = new EvalVisitor(varMap); // Evaluate expression by traversing AST
        System.out.printf("\nEval:\n%s\n%s\n", eval.visit(tree), eval.getVariableMap().toString());

        Set<String> keys = varMap.keySet();
        for (String key : keys) {
            System.out.printf("%s = %f\n", key, varMap.get(key));
        }
    }
}
