package ce305;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ExprParser}.
 */
public interface ExprListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ExprParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(ExprParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(ExprParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterStatements(ExprParser.StatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitStatements(ExprParser.StatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(ExprParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(ExprParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parentheses}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterParentheses(ExprParser.ParenthesesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parentheses}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitParentheses(ExprParser.ParenthesesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code num}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterNum(ExprParser.NumContext ctx);
	/**
	 * Exit a parse tree produced by the {@code num}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitNum(ExprParser.NumContext ctx);
	/**
	 * Enter a parse tree produced by the {@code id}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterId(ExprParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code id}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitId(ExprParser.IdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplicative}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicative(ExprParser.MultiplicativeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplicative}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicative(ExprParser.MultiplicativeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code additive}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterAdditive(ExprParser.AdditiveContext ctx);
	/**
	 * Exit a parse tree produced by the {@code additive}
	 * labeled alternative in {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitAdditive(ExprParser.AdditiveContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#assign}.
	 * @param ctx the parse tree
	 */
	void enterAssign(ExprParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#assign}.
	 * @param ctx the parse tree
	 */
	void exitAssign(ExprParser.AssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#numcond}.
	 * @param ctx the parse tree
	 */
	void enterNumcond(ExprParser.NumcondContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#numcond}.
	 * @param ctx the parse tree
	 */
	void exitNumcond(ExprParser.NumcondContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#boolcond}.
	 * @param ctx the parse tree
	 */
	void enterBoolcond(ExprParser.BoolcondContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#boolcond}.
	 * @param ctx the parse tree
	 */
	void exitBoolcond(ExprParser.BoolcondContext ctx);
}