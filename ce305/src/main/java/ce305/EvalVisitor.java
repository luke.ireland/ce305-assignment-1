package ce305;

import java.util.HashMap;

public class EvalVisitor extends ExprBaseVisitor<Double> {

    // Maps var names to expression values
    private HashMap<String, Double> varMap;

    public EvalVisitor(HashMap<String, Double> map) {
        varMap = map;
    }

    public HashMap<String, Double> getVariableMap() {
        return varMap;
    }

    @Override
    public Double visitStart(ExprParser.StartContext ctx) {
        Double result = null;
        result = this.visit(ctx.statements());
        return result;
    }

    @Override
    public Double visitStatements(ExprParser.StatementsContext ctx) {
        Double result = null;
        for (ExprParser.StatementContext e : ctx.statement()) {
            result = this.visit(e);
        }
        return result;
    }

    // Method for assigning expression value to a variable
    @Override
    public Double visitAssign(ExprParser.AssignContext ctx) {
        String id = ctx.ID().getText();
        Double value = null;
        if (ctx.numcond() != null) {
            value = this.visit(ctx.numcond());
            System.out.println(value);
        } else if (ctx.boolcond() != null) {
            value = this.visit(ctx.boolcond());
        } else if (ctx.exp() != null) {
            value = this.visit(ctx.exp());
        }
        varMap.put(id, value);
        return value;
    }

    @Override
    public Double visitMultiplicative(ExprParser.MultiplicativeContext ctx) {
        Double left = Double.valueOf(this.visit(ctx.left).toString());
        Double right = Double.valueOf(this.visit(ctx.right).toString());
        String op = ctx.op.getText();
        Double result;
        if (op.equals("*")) {
            result = multiply(left, right);
        } else {
            result = divide(left, right);
        }
        return result;
    }

    @Override
    public Double visitAdditive(ExprParser.AdditiveContext ctx) {
        Double left = Double.valueOf(this.visit(ctx.left).toString());
        Double right = Double.valueOf(this.visit(ctx.right).toString());
        String op = ctx.op.getText();
        Double result;
        if (op.equals("+")) {
            result = add(left, right);
        } else {
            result = subtract(left, right);
        }
        return result;
    }

    @Override
    public Double visitNum(ExprParser.NumContext ctx) {
        return (ctx.SUB() == null ? 1 : -1) * Double.parseDouble(ctx.NUMBER().getText());
    }

    @Override
    public Double visitParentheses(ExprParser.ParenthesesContext ctx) {
        return (ctx.SUB() == null ? 1 : -1) * this.visit(ctx.exp());
    }

    @Override
    public Double visitId(ExprParser.IdContext ctx) {
        return varMap.get(ctx.ID().getText());
    }

    // Compare values of arithmetic expressions
    @Override
    public Double visitNumcond(ExprParser.NumcondContext ctx) {
        Double left = this.visit(ctx.left);
        Double right = this.visit(ctx.right);
        String op = ctx.NUMOPS().getText();
        switch (op) {
        case "==":
            if (left.equals(right)) {
                return 1.0;
            }
            return -1.0;

        case "!=":
            if (left != right) {
                return 1.0;
            }
            return -1.0;

        case ">":
            if (left > right) {
                return 1.0;
            }
            return -1.0;

        case "<":
            if (left < right) {
                return 1.0;
            }
            return -1.0;

        case ">=":
            if (left >= right) {
                return 1.0;
            }
            return -1.0;

        case "<=":
            if (left <= right) {
                return 1.0;
            }
            return -1.0;

        default:
            throw new RuntimeException("Should not have ended up at Numcond");
        }
    }

    // Compare values of boolean expressions
    @Override
    public Double visitBoolcond(ExprParser.BoolcondContext ctx) {
        Double left = this.visit(ctx.left);
        Double right = this.visit(ctx.right);
        String op = ctx.BOOLOPS().getText();
        if (op == "&") {
            if (left == 1.0 & right == 1.0) {
                return 1.0;
            }
            return -1.0;
        } else if (op == "|") {
            if (left == 1.0 | right == 1.0) {
                return 1.0;
            }
            return -1.0;
        }
        throw new RuntimeException("Should not have ended up at Boolcond");
    }

    private Double add(Double num1, Double num2) {
        return num1 + num2;
    }

    private Double subtract(Double num1, Double num2) {
        return num1 - num2;
    }

    private Double multiply(Double num1, Double num2) {
        return num1 * num2;
    }

    private Double divide(Double num1, Double num2) {
        return num1 / num2;
    }
}
