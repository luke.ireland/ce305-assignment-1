# Report

## Design

### Tokens
My tokens were:
*   Number - Digits 0 to 9, One or More times, then an optional float addition of a decimal point and more digits
*   ID - String for variables, consists of one or more upper or lower case chars
*   MUL - Asterisk used for multiplication
*   DIV - Slash used for division
*   ADD - Plus used for addition
*   SUB - Dash used for subtraction
*   EQ - Equal comparison operator for two arithmetic expressions
*   NE - Not equal comparison operator
*   GT - Greather than comparison operator
*   LT - Less than
*   GE - Greater than or equal to
*   LE - Less than or equal to
*   AND - Boolean comparison operator
*   OR - Boolean comparison operator
*   LPAREN - Left parenthesis
*   RPAREN - Right parenthesis
*   SEMICOLON -  Used for end of statements
*   WS - Whitespace
*   NUMOPS - Any arithemtic comparison operator
*   BOOLOPS - Any boolean comparisson operator

### Syntax
My expressions were:
*   Start - Start of all expressions, consisting of 0 or more statements.
*   Statements - Multiple statements
*   Statement - Either a arithmetic comparison, a boolean comparison, an expression evaluation, or an assignment
*   Exp - Arithemtic expression to evaluate
    *   Multiplicative - Multiply or divide the left expression by the right expression
    *   Additive - Add or subtract the left expression by the right expression
    *   Parentheses - Enclose expression in brackets (optional negative)
    *   Num - Number literal (optional negative)
    *   ID -  Variable string or value
*   Assign - Evaluate arithmetic expression or arithmetic/boolean comparison, and assign value to variable
*   Numcond - Compare left and right arithmetic expression with NUMOPS operator
*   Boolops - Compare left and right boolean expression with BOOLOPS operator

## Code

### EvalVisitor
This class evaluates the given statements/expressions and outputs the result.

### PprintVisitor
This class displays the statements/expressions nicely, and acts like a kind of formatter.

### Main
This class contains a tree converter from ParseTree to JTree, which is then displayed in a JFrame and builds the ParseTree from using the input file to create tokens via a lexer, then parse those tokens to build an AST in the form of a ParseTree.

That ParseTree is visited by EvalVisitor and PprintVisitor to output the appropriate result.

## Conclusion
I decided to make the number support both integer (no decimal point) and float (with decimal point), with just one data type (number) for simplicity reasons.

When deciding on how to visually display the AST, I originally chose JTree as it's the most obvious choice to use with JFrame/Swing, but it's clearly not very appealing or useful. I then used Antlr's GUI library and instead added the TreeViewer object to the JFrame instead.

I had to use C-style bools (1 and -1), as I wasn't sure on how to pass booleans through the eval visitor. My first idea was to make a seperate visitor which deals with booleans, and use my double visitor call use that visitor only when necessary. However, I face the same issue, as I'd still have to find a way to use that evaluation and pass it back up the tree to the root node. I also thought about using a visitor that returns and object, which can be either a boolean or double, but I hit a snag once more when I kept getting nulls returned, simply by changing the return type of the function I'm calling/visiting.