from pathlib import Path

package_string = "package ce305;\n"
src_path = Path(__file__).parent.joinpath(
    "src","main","java/ce305"
)
files = [f for f in src_path.glob("*.java")]

for file in files:
    lines = []
    with file.open() as read_file:
        lines = read_file.readlines()
    if package_string not in lines[0]:
        lines[0] = package_string
        with file.open('w') as write_file:
            write_file.writelines(lines)